#! /bin/bash

set -x

export PATH=$PATH:/usr/local/bin

type gem > /dev/null 2>&1

if [ $? -ne 0 ]; then
  echo "gem no installed"
  exit 10
fi

gem install rails --no-ri --no-rdoc
